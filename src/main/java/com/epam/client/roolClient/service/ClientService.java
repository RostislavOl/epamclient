/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.client.roolClient.service;

import com.epam.client.roolClient.entities.Client;
import com.epam.client.roolClient.repository.ClientOrderRepository;
import com.epam.client.roolClient.repository.ClientRepository;
import com.epam.client.roolClient.repository.ClientTypeRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ClientService {
    
    private final ClientRepository clientRepo;
    
    /*
    ClientTypeRepository clientTypeRepo;
    
    ClientOrderRepository coRepo;*/
    
    public List<Client> getClients(){
        return clientRepo.findAll();
    }
    
    public Client getClientById(Long id){
        Client client = clientRepo.findById(id).orElse(new Client());
        return client;
    }
    
    @Transactional
    public Client createClient(Client client){
        return clientRepo.save(client);
    }
    
    @Transactional
    public void deleteClient(Long clientID){
        clientRepo.deleteById(clientID);
    }
    
    @Transactional
    public Client updateClient(Client client){
        if (client != null) {
            Client clientInRepository = getClientById(client.getId());
            if (clientInRepository != null) {
                clientRepo.deleteById(clientInRepository.getId());
                clientInRepository.setShortName(client.getShortName());
                clientInRepository.setFullName(client.getFullName());
                clientInRepository.setClienttype(client.getClienttype());
                clientInRepository.setInn(client.getInn());
                clientInRepository.setOkpo(client.getOkpo());
                clientInRepository.setCreationDate(client.getCreationDate());
                clientInRepository.setModificationDate(client.getModificationDate());
                clientRepo.save(clientInRepository);
                return client;
            } else {
                clientRepo.save(client);
            }
        }
        return null;
    }
    
}
