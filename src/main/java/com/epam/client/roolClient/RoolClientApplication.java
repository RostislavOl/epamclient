package com.epam.client.roolClient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoolClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoolClientApplication.class, args);
	}

}
