/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.client.roolClient.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "client_type")
public class ClientTypes implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Client id;
    
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="clienttype")
    private Client client;
    
    @Column
    private String shortName;
    
    @Column
    private String fullName;
    
    @Column
    private String clientTypeCode;
    
}
