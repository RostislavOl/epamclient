/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.client.roolClient.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@NoArgsConstructor
@Table(name ="client")
public class Client implements Serializable {
    
    @Id
    @Column
    private long id;
    
    @OneToMany(mappedBy="id")
    private List<Order> orders;
    
    @Column
    private String shortName; //
    
    @Column
    private String fullName; //
    
    @OneToOne(cascade = CascadeType.PERSIST, mappedBy = "client")
    private ClientTypes clienttype; //
    
    @Column
    private String inn; //
    
    @Column
    private String okpo; //
    
    //private List<Order> orders;
    
    @Column(name="creationdate")
    private Date creationDate;
    
    @Column(name="modificationdate")
    private Date modificationDate;
    
}
