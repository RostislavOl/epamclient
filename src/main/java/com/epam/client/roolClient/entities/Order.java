/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.client.roolClient.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@NoArgsConstructor
@Table(name ="orders")
public class Order implements Serializable {
    
    @Id
    @Column
    private Long id;
    
    @ManyToOne
    @JoinColumn(name="orders")
    private Client client;
    
    @Column
    private String goodsName;
    
    @Column(name="eat")
    private Enum category;
    
    @Column
    private long clientID; 
    
}
