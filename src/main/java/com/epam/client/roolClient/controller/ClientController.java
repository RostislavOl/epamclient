/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.client.roolClient.controller;

import com.epam.client.roolClient.entities.Client;
import com.epam.client.roolClient.service.ClientService;
import java.net.URI;
import java.net.URISyntaxException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/client")
@RequiredArgsConstructor
public class ClientController {
    
    private final ClientService service;
    
    @GetMapping("/allClients")
    public ResponseEntity<?> getClients(){
        return ResponseEntity.status(HttpStatus.OK).body(service.getClients());
    }
    
    @GetMapping("/singleClient/{clientID}")
    public ResponseEntity<?> getSingleClient(@PathVariable Long clientID){
        return ResponseEntity.status(HttpStatus.OK).body(service.getClientById(clientID));
    }
    
    @PostMapping("/newUser")
    public ResponseEntity<?> createClient(@RequestBody Client client){
        return ResponseEntity.status(HttpStatus.OK).body(service.createClient(client));
    }
    
    @PutMapping("/updateClient")
    public ResponseEntity<?> updateClient(@RequestBody Client client){
        service.updateClient(client);
        try {
            return ResponseEntity
                    .created(new URI("/singleClient/" + client.getId()))
                    .body(client);
        } catch (URISyntaxException e) {
            return ResponseEntity.status(HttpStatus.CREATED).body(client);
        }
    }
    
    @DeleteMapping("/deleteClient/{clientID}")
    public void deleteClient(@PathVariable Long clientID){
        service.deleteClient(clientID);
    }
    
}
